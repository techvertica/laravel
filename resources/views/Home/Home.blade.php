@extends('layouts.master')
@section('title','Home')
@section('slider')
@include('layouts.slider')
@show
@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1>Half Slider by Start Bootstrap</h1>
        <p>The background images for the slider are set directly in the HTML using inline CSS. The rest of the styles for this template are contained within the <code>half-slider.css</code>file.</p>
    </div>
</div>
@stop
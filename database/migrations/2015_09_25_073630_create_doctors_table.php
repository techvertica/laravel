<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('doctors', function (Blueprint $table) {
            $table->increments('docid');
            $table->inetger('is_delete');
            $table->string('empid', 225);
            $table->string('name', 225);
            $table->string('emailid', 225);
            $table->string('mobile', 225);
            $table->string('area', 225);
            $table->string('speciality', 225);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('doctors');
    }

}

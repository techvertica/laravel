@extends('layouts.master')
@section('title','List Doctor')
@section('content')
<div class="row" style="margin-top: 150px">
    <h1>Doctor List</h1>
    <div class="col-lg-12">
        <table class="table table-bordered">
            <tr>
                <th>Name</th>
                <th>Specility</th>
                <th>Mobile</th>
                <th>Emailid</th>
                <th>Area</th>
            </tr>
            @foreach($doctors as $doctor)
            <tr>
                <td>{{$doctor->name}}</td>
                <td>{{$doctor->speciality}}</td>
                <td>{{$doctor->mobile}}</td>
                <td>{{$doctor->emailid}}</td>
                <td>{{$doctor->area}}</td>
            </tr>
            @endforeach
        </table>
    </div>
</div>
<div class="row" >
    <div class="col-lg-12">
        {!! str_replace('/?', '?', $doctors->render()) !!}
        <?php //echo $doctors->render() ?>
    </div>
</div>
@stop

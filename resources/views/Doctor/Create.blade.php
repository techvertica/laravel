@extends('layouts.master')
@section('title','Add Doctor')
@section('content')
<div class="row" style="margin-top: 150px">
    <h1>Add Doctor</h1>
    <div class="col-lg-12">
        {!! Form::open(['action' => 'DoctorController@save', 'method' => 'post']) !!}

        <div class="form-group">
            {!! Form::label('title', 'Name:', ['class' => 'control-label']) !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('title', 'Speciality:', ['class' => 'control-label']) !!}
            {!! Form::text('speciality', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('title', 'Emailid:', ['class' => 'control-label']) !!}
            {!! Form::text('emailid', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('title', 'Mobile:', ['class' => 'control-label']) !!}
            {!! Form::text('mobile', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('title', 'Area:', ['class' => 'control-label']) !!}
            {!! Form::text('area', null, ['class' => 'form-control']) !!}
        </div>

        {!! Form::submit('Create New Task', ['class' => 'btn btn-primary']) !!}

        {!! Form::close() !!}
    </div>
</div>
@stop
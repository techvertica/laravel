<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model {

    protected $table = 'doctors';
    public $timestamps = false;
    protected $fillable = ['name', 'emailid', 'speciality', 'mobile', 'area'];

    static function listAll() {
        return Doctor::paginate(1000);
    }

}

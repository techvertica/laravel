<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Doctor;

class DoctorController extends Controller {

    function create() {
        return view('Doctor.Create');
    }

    function save(Request $request) {
        //dd($request);
        $input = $request->all();
        Doctor::create($input);
        return redirect()->back();
    }

    function listAll() {
        $doctors = Doctor::listAll();
        return view('Doctor.list')->with('doctors', $doctors);
    }

}
